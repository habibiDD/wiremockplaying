import com.google.gson.Gson;
import org.json.JSONException;

import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by abdur on 7/10/2020.
 */
public class JsonReadExample {

    public static void main(String[] args) throws JSONException, FileNotFoundException {

        Input input = new Input();
        Gson gson = new Gson();

/*         SOLUTION 1

            try {
            Object obj = new JSONParser().parse(new FileReader("input.json"));
            JSONObject jo = (JSONObject) obj;
            String firstName = (String) jo.get("firstName");
            String lastName = (String) jo.get("lastName");

            input.setFirstName(firstName);
            System.out.println(firstName);
            input.setLastName(lastName);
            System.out.println(lastName);

            Map address  = ((Map)jo.get("address"));
            Address myAddress = new Address();

            myAddress.setStreetAddress((String) address.get("streetAddress"));
            myAddress.setCity((String) address.get("city"));
            myAddress.setState((String) address.get("state"));
            myAddress.setPostalCode(((Long)address.get("postalCode")).intValue());
            System.out.println(myAddress);
            input.setAddress(myAddress);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }*/

        input = gson.fromJson(new FileReader("input.json"),Input.class);

        System.out.println(input);

    }
}
